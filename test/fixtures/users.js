import {
  READONLY_USER_USERNAME,
  READONLY_USER_PASSWORD,
  READONLY_ROLE,
  READWRITE_USER_USERNAME,
  READWRITE_USER_PASSWORD,
  READWRITE_ROLE,
} from './constants.js';

export const readOnlyUser = {
  type: 'user',
  name: READONLY_USER_USERNAME,
  password: READONLY_USER_PASSWORD,
  roles: [READONLY_ROLE],
};

export const readWriteUser = {
  type: 'user',
  name: READWRITE_USER_USERNAME,
  password: READWRITE_USER_PASSWORD,
  roles: [READWRITE_ROLE],
};
