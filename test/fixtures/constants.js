export const TEST_DB_NAME = 'relax_test_db';

export const REMOTE_PROTOCOL = 'http';

export const REMOTE_HOST = 'localhost:6984';

export const REMOTE_URL = `${REMOTE_PROTOCOL}://${REMOTE_HOST}`;

export const REMOTE_DB = `${REMOTE_URL}/${TEST_DB_NAME}`;

export const LOCAL_DB = TEST_DB_NAME;

export const READONLY_USER_USERNAME = 'relax';

export const READONLY_USER_PASSWORD = 'relax';

export const READONLY_ROLE = 'readonly';

export const READWRITE_USER_USERNAME = 'editor';

export const READWRITE_USER_PASSWORD = 'editor';

export const READWRITE_ROLE = 'readwrite';
