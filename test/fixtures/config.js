import { READONLY_ROLE, READWRITE_ROLE } from './constants.js';

export const defaultSecurity = {
  admins: {
    names: [],
    roles: [],
  },
  members: {
    names: [],
    roles: [READONLY_ROLE, READWRITE_ROLE],
  },
};
