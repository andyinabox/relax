#!/bin/bash

npx pouchdb-server -p 6984 -m -c test/pouchdb-server.config.json &
PID=$!
npx karma start
kill $PID