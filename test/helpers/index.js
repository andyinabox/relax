import uuid from 'uuid/dist/esm-browser/v4';
import pdb from 'pouchdb-browser';
import find from 'pouchdb-find';
import auth from 'pouchdb-authentication';
import {
  READONLY_USER_PASSWORD,
  READONLY_USER_USERNAME,
  REMOTE_URL,
} from '../fixtures/constants.js';

pdb.plugin(find).plugin(auth);

export const PouchDB = pdb;
export const getUuid = uuid;

export const ensureTestDbIsRunning = async () => {
  try {
    const resp = await fetch(REMOTE_URL);
    if (!resp.ok) {
      throw new Error(
        `expected 200 response from ${REMOTE_URL}, got ${resp.status}`
      );
    }
  } catch (err) {
    throw new Error(`Test DB does not appear to be running! ${err}`);
  }
};

export const newReadOnlyUserRemoteDb = () => {
  return new PouchDB(getUuid(), {
    auth: {
      username: READONLY_USER_USERNAME,
      password: READONLY_USER_PASSWORD,
    },
  });
};

export const newRemoteDb = () => {
  return new PouchDB(getUuid());
};
export const newLocalDB = () => {
  return new PouchDB(getUuid());
};

export const getChangesStreamMock = () =>
  Object.freeze({
    value: () => {},
    cancel: () => {},
    watch: () => {},
    errors: () => {},
    filter: () => {},
  });

export const getAuthMock = () =>
  Object.freeze({
    authenticated: () => false,
    watch: () => {},
    login: () => {},
    logout: () => {},
    check: () => {},
  });

export const getStoreMock = () =>
  Object.freeze({
    local: () => newLocalDB(),
    remote: () => newRemoteDb(),
    publish: async () => Promise.resolve(),
    revertAll: async () => Promise.resolve(),
    listChanges: async () => Promise.resolve(),
    changesStream: getChangesStreamMock(),
    auth: getAuthMock(),
  });

export const sleep = async t => {
  return new Promise(r => {
    window.setTimeout(r, t);
  });
};
