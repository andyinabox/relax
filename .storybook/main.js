module.exports = {
  stories: ['../src/**/*.stories.{js,md,mdx}'],
  addons: [
    'storybook-prebuilt/addon-knobs/register.js',
    'storybook-prebuilt/addon-docs/register.js',
    'storybook-prebuilt/addon-viewport/register.js',
    'storybook-prebuilt/addon-actions/register.js',
  ],
  esDevServer: {
    // custom es-dev-server options
    nodeResolve: true,
    moduleDirs: ['web_modules', 'node_modules'],
    watch: true,
    open: true,
  },
};
