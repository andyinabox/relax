

export const isAuthError = err => err.error && err.error === "unauthorized"