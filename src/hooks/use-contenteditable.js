import { hook, Hook, useMemo, useEffect, useState } from '../index.js';

export const useContenteditable = hook(
  class extends Hook {
    // everything after `state` is an argument passed to your hook
    constructor(id, state) {
      super(id, state);
      this.el = null;
      this.onValueInput = null;
    }

    update(initialValue, selector) {
      // store the current edited value
      const [value, setValue] = useState(initialValue);

      const getEditedContent = () => this.el.innerHTML;
      const setEditedContent = html => {
        this.el.innerHTML = html;
      };

      // update both the edited value
      // and the contents of the contenteditable element
      const setValueHard = val => {
        setValue(val);
        setEditedContent(val);
      };

      // set the contenteditble element
      // and add listener for input
      useEffect(() => {
        // get the el
        this.el = this.state.host.shadowRoot.querySelector(selector);
        // create the input event handler
        this.onValueInput = () => setValue(getEditedContent());
        // add event listener
        if (this.el) this.el.addEventListener('input', this.onValueInput, true);
      }, []);

      // has the content been edited
      const hasEdits = useMemo(() => value !== initialValue, [
        value,
        initialValue,
      ]);

      return [value, setValueHard, hasEdits];
    }

    teardown() {
      this.el.removeEventListener('input', this.onValueInput);
    }
  }
);
