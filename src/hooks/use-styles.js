import { supportsAdoptingStyleSheets } from 'lit-element/lib/css-tag';
import { hook, Hook, useLayoutEffect } from '../index.js';

export const useStyles = hook(
  class extends Hook {
    // everything after `state` is an argument passed to your hook

    adoptStyles = (el, styles) => {
      if (styles.length === 0) {
        return;
      }

      // There are three separate cases here based on Shadow DOM support.
      // (1) shadowRoot polyfilled: use ShadyCSS
      // (2) shadowRoot.adoptedStyleSheets available: use it
      // (3) shadowRoot.adoptedStyleSheets polyfilled: append styles after
      // rendering
      if (window.ShadyCSS !== undefined && !window.ShadyCSS.nativeShadow) {
        window.ShadyCSS.ScopingShim.prepareAdoptedCssText(
          styles.map(s => s.cssText),
          el.localName
        );
      } else if (supportsAdoptingStyleSheets) {
        // eslint-disable-next-line no-param-reassign
        el.shadowRoot.adoptedStyleSheets = styles.map(s =>
          s instanceof CSSStyleSheet ? s : s.styleSheet
        );
      } else {
        styles.forEach(s => {
          const style = document.createElement('style');
          style.textContent = s.cssText;
          el.shadowRoot.appendChild(style);
        });
      }
    };

    update(...rawStyles) {
      // filter out any undefined styles
      const styles = rawStyles.filter(s => !!s);
      useLayoutEffect(() => this.adoptStyles(this.state.host, styles), [
        styles,
      ]);
    }
  }
);
