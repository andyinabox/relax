import { html } from 'lit-html';

import './rlx-app-login.js';

export default {
  title: 'App|Login',
  component: 'rlx-app-login',
};

export const loginButton = () => html`<rlx-app-login></rlx-app-login>`;
