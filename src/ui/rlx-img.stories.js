import { html } from 'lit-html';

import './rlx-img.js';

export default {
  title: 'Elements|Img',
  component: 'rlx-img',
};

export const imageElement = () =>
  html`<div style="width: 100vh;">
    <rlx-img src="https://picsum.photos/800/600" alt="alt text"></rlx-img>
  </div>`;
