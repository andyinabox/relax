import { component, html, css } from '../index.js';
import { useStyles } from '../hooks/index.js';

const defaultStyles = css``;

export function RichTextEditor({ styles }) {
  useStyles(defaultStyles, styles);

  return html` <div class="wrap"></div> `;
}

customElements.define(
  'rlx-rte',
  component(RichTextEditor, {
    observedAttributes: [],
  })
);
