import { html } from 'lit-html';
import { withKnobs, withWebComponentsKnobs } from '@open-wc/demoing-storybook';

import './rlx-app-admin.js';

export default {
  title: 'App|Admin',
  decorators: [withKnobs, withWebComponentsKnobs],
  component: 'rlx-app-admin',
};

export const loggedOut = () => html`<rlx-app-admin></rlx-app-admin>`;

export const loggedIn = () =>
  html`<rlx-app-admin authenticated></rlx-app-admin>`;
