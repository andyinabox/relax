import { component, html, css } from '../index.js';
import { useStyles } from '../hooks/index.js';
import { loginEvent } from '../events/index.js';

const defaultStyles = css`
  .wrap {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(0, 0, 0, 0.75);
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 999;
  }
  .login {
    background: white;
    padding: 1rem;
    display: flex;
    flex-direction: column;
  }
  input {
    margin-bottom: 0.25rem;
  }
`;

export function RelaxAppLogin({ styles }) {
  useStyles(defaultStyles, styles);

  const onSubmit = evt => {
    evt.preventDefault();
    const user = this.shadowRoot.getElementById('username').value;
    const password = this.shadowRoot.getElementById('password').value;
    this.dispatchEvent(loginEvent(user, password));
  };

  const onClose = () => {
    this.dispatchEvent(new CustomEvent('close', {}));
  };

  const onKeyUp = async evt => {
    switch (evt.keyCode) {
      // esc key
      case 27:
        onClose(evt);
        break;
      case 13:
        onSubmit(evt);
        break;
      default:
        break;
    }
  };

  return html`
    <div class="wrap">
      <div class="login" @keyup=${onKeyUp}>
        <input type="text" placeholder="username" id="username" />
        <input type="password" id="password" />
        <input type="submit" value="Login" id="submit" @click=${onSubmit} />
      </div>
    </div>
  `;
}

customElements.define(
  'rlx-app-login',
  component(RelaxAppLogin, {
    observedAttributes: [],
  })
);
