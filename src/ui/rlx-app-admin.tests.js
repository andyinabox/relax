import { expect, fixture } from '@open-wc/testing';
import { loggedOut, loggedIn } from './rlx-app-admin.stories.js';

describe('<rlx-app-admin>', async () => {
  it('should only display the login button when logged out', async () => {
    const el = await fixture(loggedOut());

    expect(el.shadowRoot.querySelector('#login')).to.exist;
    expect(el.shadowRoot.querySelector('#publish')).to.not.exist;
    expect(el.shadowRoot.querySelector('#revert')).to.not.exist;
    expect(el.shadowRoot.querySelector('#logout')).to.not.exist;
  });

  it('should display all buttons except login when logged in', async () => {
    const el = await fixture(loggedIn());

    expect(el.shadowRoot.querySelector('#login')).to.not.exist;
    expect(el.shadowRoot.querySelector('#publish')).to.exist;
    expect(el.shadowRoot.querySelector('#revert')).to.exist;
    expect(el.shadowRoot.querySelector('#logout')).to.exist;
  });
});
