import { html } from 'lit-html';

import './rlx-doc-edit.js';

export default {
  title: 'Doc|Edit',
  component: 'rlx-doc-edit',
};

export const defaultView = () => html`<rlx-doc-edit></rlx-doc-edit>`;
