/* eslint-disable lit/no-value-attribute */
import {
  component,
  html,
  useState,
  useEffect,
  useMemo,
  css,
} from '../index.js';
import { useStyles } from '../hooks/index.js';
import { tryPreloadImg } from '../util/index.js';

const defaultStyles = css`
  .wrap {
    cursor: text;
    position: relative;
    background: #eee;
  }
  .wrap.loading {
    cursor: progress;
    height: 0;
    overflow: hidden;
    padding-top: calc((6 / 8) * 100%);
  }
  .wrap.loading > img {
    visibility: hidden;
  }
  .wrap.loading > input {
    cursor: progress;
  }

  img {
    width: 100%;
  }
  input {
    position: absolute;
    opacity: 0.8;
    top: 50%;
    left: 50%;
    height: 2rem;
    padding: 0.25rem;
    font-size: 1rem;
    transform: translate(-50%, -50%);
  }
`;

export function RelaxImg({ src, alt, styles }) {
  useStyles(defaultStyles, styles);

  const [url, setUrl] = useState(src);
  const [isEditing, setIsEditing] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const revertInput = () => {
    setIsEditing(false);
    setUrl(src);
  };

  const loadUrl = async evt => {
    const el = evt.originalTarget;
    const newUrl = el.value;
    setIsLoading(true);
    try {
      await tryPreloadImg(newUrl);
      setUrl(newUrl);
      setIsEditing(false);
      setIsLoading(false);
    } catch (e) {
      setUrl(src);
      el.value = src;
      setIsLoading(false);
    }
  };

  const onKeyUp = async evt => {
    switch (evt.keyCode) {
      // esc key
      case 27:
        revertInput();
        break;
      case 13:
        loadUrl(evt);
        break;
      default:
        break;
    }
  };

  const loadingCls = useMemo(() => (isLoading ? 'loading' : ''), [isLoading]);

  const renderInput = () => {
    if (!isEditing) return null;
    return html`<input
      type="text"
      ?disabled=${isLoading}
      value=${url}
      @keyup=${onKeyUp}
    />`;
  };

  // image src has changed
  useEffect(() => {
    setIsLoading(true);
    const img = new Image();
    img.onload = () => {
      setUrl(src);
      setIsLoading(false);
    };
    img.src = src;
  }, [src]);

  // local url state has changed
  useEffect(() => {
    this.dispatchEvent(new CustomEvent('change', { detail: url }));
  }, [url]);

  return html`
    <div class="wrap ${loadingCls}" @click=${() => setIsEditing(true)}>
      ${renderInput()}
      <img src=${url} alt=${alt} />
    </div>
  `;
}

customElements.define(
  'rlx-img',
  component(RelaxImg, {
    observedAttributes: ['src', 'alt'],
  })
);
