import { component, html, useMemo, css } from '../index.js';
import { useStyles } from '../hooks/index.js';
import { publishEvent, revertAllEvent, logoutEvent } from '../events/index.js';

const defaultStyles = css`
  #wrap {
    text-align: right;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    background: white;
  }
`;

export function RelaxAppAdmin({ authenticated, changesCount, styles }) {
  useStyles(defaultStyles, styles);

  const hasChanges = useMemo(() => changesCount > 0, [changesCount]);
  const changesDisplay = useMemo(
    () => (hasChanges ? `(${changesCount})` : ''),
    [changesCount]
  );
  const onPublish = () => this.dispatchEvent(publishEvent());
  const onRevert = () => this.dispatchEvent(revertAllEvent());
  const onLogin = () => this.dispatchEvent(new CustomEvent('login', {}));
  const onLogout = () => this.dispatchEvent(logoutEvent());

  const renderButtons = () => {
    if (authenticated) {
      return html`
        <button id="logout" @click=${onLogout}>Logout</button>
        <button
          id="revert"
          ?disabled=${!hasChanges}
          class="delete"
          @click=${onRevert}
        >
          Revert All
        </button>
        <button
          id="publish"
          ?disabled=${!hasChanges}
          class="submit"
          @click=${onPublish}
        >
          Publish ${changesDisplay}
        </button>
      `;
    }

    return html`<button id="login" @click=${onLogin}>Login</button>`;
  };

  return html` <div id="wrap">${renderButtons()}</div> `;
}

customElements.define(
  'rlx-app-admin',
  component(RelaxAppAdmin, {
    observedAttributes: ['authenticated'],
  })
);
