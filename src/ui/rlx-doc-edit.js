import { component, html, css } from '../index.js';
import {
  docSaveEvent,
  docDeleteEvent,
  docRevertEvent,
} from '../events/index.js';
import { useStyles } from '../hooks/index.js';

const defaultStyles = css`
  .wrap {
    text-align: right;
  }
`;

export function RelaxDocEdit({
  deleteText = 'Delete',
  revertText = 'Revert',
  commitText = 'Commit',
  hideDelete,
  hideRevert,
  hideCommit,
  doc,
  styles,
}) {
  useStyles(defaultStyles, styles);

  const onRevert = () => {
    this.dispatchEvent(docRevertEvent(doc));
  };
  const onCommit = () => {
    this.dispatchEvent(docSaveEvent(doc));
  };
  const onDelete = () => {
    this.dispatchEvent(docDeleteEvent(doc));
  };

  const deleteBtn = () => {
    if (hideDelete) return null;
    return html`<button class="delete" @click=${onDelete}>
      ${deleteText}
    </button>`;
  };
  const commitBtn = () => {
    if (hideCommit) return null;
    return html`<button class="submit" @click=${onCommit}>
      ${commitText}
    </button>`;
  };
  const revertBtn = () => {
    if (hideRevert) return null;
    return html`<button @click=${onRevert}>${revertText}</button>`;
  };

  return html`
    <div class="wrap">${deleteBtn()} ${revertBtn()} ${commitBtn()}</div>
  `;
}

customElements.define(
  'rlx-doc-edit',
  component(RelaxDocEdit, {
    observedAttributes: [
      'delete-text',
      'commit-text',
      'revert-text',
      'hide-delete',
      'hide-revert',
      'hide-commit',
    ],
  })
);
