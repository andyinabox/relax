export const docSaveEvent = doc =>
  new CustomEvent('doc-save', {
    bubbles: true,
    composed: true,
    detail: doc,
  });

export const docDeleteEvent = doc =>
  new CustomEvent('doc-delete', {
    bubbles: true,
    composed: true,
    detail: doc,
  });

export const docRevertEvent = doc =>
  new CustomEvent('doc-revert', {
    bubbles: true,
    composed: true,
    detail: doc,
  });

export const publishEvent = () =>
  new CustomEvent('publish', {
    bubbles: true,
    composed: true,
  });

export const revertAllEvent = () =>
  new CustomEvent('revert-all', {
    bubbles: true,
    composed: true,
  });

export const loginEvent = (username, password) =>
  new CustomEvent('user-login', {
    bubbles: true,
    composed: true,
    detail: {
      username,
      password,
    },
  });

export const logoutEvent = () =>
  new CustomEvent('user-logout', {
    bubbles: true,
    composed: true,
  });
