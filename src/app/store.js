import { createDocStream } from './doc-stream.js';
import { createAuth } from './auth.js';
import { createDiffer } from './differ.js';
import { createChangesStream } from './changes-stream.js';

const defaults = {
  statusDocId: '_local/status',
  publicUsername: 'relax',
  publicPassword: 'relax',
};

// get the last sequence # in the local db
const getLastSequence = async db => {
  // eslint-disable-next-line camelcase
  const { update_seq } = await db.info();
  return parseInt(update_seq, 10);
};

export const createStore = (PouchDB, localName, remoteUrl, options = {}) => {
  const { statusDocId, publicUsername, publicPassword } = {
    ...defaults,
    ...options,
  };

  const local = new PouchDB(localName);
  const remote = new PouchDB(remoteUrl, {
    skip_setup: true,
    auth: { username: publicUsername, password: publicPassword },
  });
  const auth = createAuth(remote);

  // set up local document watcher
  const changesStream = createChangesStream(local);

  // set up streams
  const statusStream = createDocStream(local, changesStream, {
    _id: statusDocId,
    checkPoint: 0,
  });
  statusStream.errors(err => {
    throw err;
  });

  // differ
  const differ = createDiffer(local, remote);

  auth.watch(async isAuth => {
    if (isAuth) {
      await Promise.all([remote.replicate.to(local), statusStream.ready()]);
      const { checkPoint } = statusStream.value();
      if (!checkPoint) {
        const seq = await getLastSequence(local);
        await statusStream.set({ checkPoint: seq });
      }
    }
  });

  function store() {
    return auth.authenticated() ? local : remote;
  }

  // add methocs
  Object.assign(store, {
    local: () => local,
    remote: () => remote,

    // initialization
    setup: async () => auth.check(),

    publish: async () => {
      await local.sync(remote);
      const seq = await getLastSequence(local);
      await statusStream.set({ checkPoint: seq });
    },

    revertAll: async () => {
      const { checkPoint } = statusStream.value();
      await differ.revert(checkPoint);
      const seq = await getLastSequence(local);
      await statusStream.set({ checkPoint: seq });
    },

    listChanges: async () => {
      const { checkPoint } = statusStream.value();
      return differ.list(checkPoint);
    },

    changesStream,
    auth,
  });

  return Object.freeze(store);
};
