import flyd from 'flyd';
import {
  createStreamWatcher,
  createStreamFilter,
  compareRevs,
} from '../util/index.js';

export const onChangeRegistered = async (
  doc,
  changesStream,
  timeout = 1500
) => {
  if (!doc._id) throw new Error('document is missing id');

  return new Promise((resolve, reject) => {
    if (timeout > 0) {
      window.setTimeout(() => {
        reject(new Error('change listener timed out'));
      }, timeout);
    }

    changesStream.filter(
      d => doc._id === d._id && compareRevs(doc, d),
      resolve
    );
  });
};

export const createChangesStream = db => {
  // create empty stream
  const errorsStream = flyd.stream();
  const changesStream = flyd.stream();

  // subscribe to db changes
  const changes = db
    .changes({
      live: true,
      since: 'now',
      include_docs: true,
    })
    .on('change', ({ doc }) => changesStream(doc))
    .on('denied', errorsStream)
    .on('error', errorsStream)
    .on('paused', errorsStream);

  const value = () => changesStream();
  const cancel = () => changes.cancel();
  const watch = createStreamWatcher(changesStream);
  const errors = createStreamWatcher(errorsStream);
  const filter = createStreamFilter(changesStream);

  return Object.freeze({
    value,
    cancel,
    watch,
    errors,
    filter,
  });
};
