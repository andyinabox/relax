import flyd from 'flyd';
import { createStreamWatcher } from '../util/index.js';

// this will only update the value stream if it's
// different from the current value
export const createMutationStream = initValue => {
  const value = flyd.stream(initValue);
  const setValue = v => (v !== value() ? value(v) : value());

  // return [value, setValue]
  return Object.freeze({
    value: () => value(),
    set: setValue,
    watch: createStreamWatcher(value),
  });
};
