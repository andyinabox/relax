import { expect } from '@open-wc/testing';
import { createApp } from './app.js';

import {
  ensureTestDbIsRunning,
  getUuid,
  PouchDB,
} from '../../test/helpers/index.js';
import {
  REMOTE_PROTOCOL,
  REMOTE_HOST,
  READONLY_USER_USERNAME,
  READONLY_USER_PASSWORD,
} from '../../test/fixtures/constants.js';

describe('app', async () => {
  let options;

  before(async () => {
    await ensureTestDbIsRunning();
  });

  beforeEach(async () => {
    options = {
      dbName: getUuid(),
      remote: {
        protocol: REMOTE_PROTOCOL,
        host: REMOTE_HOST,
      },
      publicUsername: READONLY_USER_USERNAME,
      publicPassword: READONLY_USER_PASSWORD,
    };
  });

  it('should be able to be created', async () => {
    const app = createApp(PouchDB, options);
    expect(app).to.exist;
  });
});
