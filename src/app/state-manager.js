import flyd from 'flyd';
import merge from 'mergerino';
import { createStreamWatcher, createStreamFilter } from '../util/index.js';

export const createStateManager = initialState => {
  // create our streams
  const patch = flyd.stream();
  const states = flyd.scan(merge, initialState, patch);
  const watch = createStreamWatcher(states);
  const filter = createStreamFilter(states);
  const value = () => states();

  // return values
  return Object.freeze({
    value,
    patch,
    watch,
    filter,
  });
};
