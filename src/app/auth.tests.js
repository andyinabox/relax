import { expect } from '@open-wc/testing';
import { isUserAuthorized, createAuth } from './auth.js';
import {
  ensureTestDbIsRunning,
  newRemoteDb,
} from '../../test/helpers/index.js';

describe('auth::isUserAuthorized', () => {
  const allowedUsers = ['allowed-user'];
  const allowedRoles = ['allowed-role', 'another-allowed-role'];
  const authorized = [
    {
      name: 'allowed-user',
      roles: [],
    },
    {
      name: 'user',
      roles: ['allowed-role'],
    },
    {
      name: 'user',
      roles: ['another-allowed-role'],
    },
    {
      name: 'user',
      roles: ['role', 'allowed-role'],
    },
  ];
  const unAuthorized = [
    undefined,
    {
      name: 'user',
      roles: ['role'],
    },
    {
      name: 'user',
      roles: [],
    },
  ];

  it('should be authorized', () => {
    authorized.forEach(user => {
      const auth = isUserAuthorized(user, allowedRoles, allowedUsers);
      expect(auth).to.equal(true);
    });
  });

  it('should be unauthorized', () => {
    unAuthorized.forEach(user => {
      const auth = isUserAuthorized(user, allowedRoles, allowedUsers);
      expect(auth).to.equal(false);
    });
  });
});

describe('auth module', async () => {
  let db;

  before(async () => {
    await ensureTestDbIsRunning();
  });

  beforeEach(async () => {
    if (db) {
      await db.destroy();
    }
    db = newRemoteDb();
  });

  it('should be able to be created', async () => {
    const auth = createAuth(db);
    expect(auth).to.exist;
  });
});
