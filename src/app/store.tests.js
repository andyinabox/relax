import { expect } from '@open-wc/testing';
import { createStore } from './store.js';
import { LOCAL_DB, REMOTE_DB } from '../../test/fixtures/constants.js';
import {
  PouchDB,
  ensureTestDbIsRunning,
  newRemoteDb,
} from '../../test/helpers';

describe('data store', async () => {
  before(async () => {
    await ensureTestDbIsRunning();
  });

  it('should be able to be created', async () => {
    const store = createStore(PouchDB, LOCAL_DB, REMOTE_DB);
    expect(store).to.exist;
  });
});
