import { isEmpty, isNumber } from '../util/index.js';

export const createDiffer = (local, remote) => {
  const diff = async seq => {
    if (!isNumber(seq)) throw new Error(`invalid sequence: ${seq}`);

    const changed = await local.changes({ since: seq });
    const revs = {};
    // convert change results to format recieved by diff
    changed.results.forEach(({ id, changes }) => {
      revs[id] = changes.map(d => d.rev);
    });
    return remote.revsDiff(revs);
  };

  const revert = async seq => {
    const currentDiff = await diff(seq);
    const ids = Object.keys(currentDiff);

    // get latest docs from both remote and local
    const results = await Promise.all([
      remote.allDocs({ include_docs: true, keys: ids }),
      local.allDocs({ include_docs: true, keys: ids }),
    ]);

    const [remoteResults, localResults] = results;
    const updates = [];

    ids.forEach((id, index) => {
      const remoteEntry = remoteResults.rows[index];
      const localEntry = localResults.rows[index];

      // doc is present on both, updated locally
      if (remoteEntry.doc && localEntry.doc) {
        updates.push({ ...remoteEntry.doc, _rev: localEntry.value.rev });

        // doc was deleted from local
      } else if (remoteEntry.doc && !localEntry.doc) {
        updates.push({
          ...remoteEntry.doc,
          _rev: localEntry.value.rev,
          _deleted: false,
        });

        // doc exists locally but not on remote
      } else if (!remoteEntry.doc && localEntry.doc) {
        updates.push({ ...localEntry.doc, _deleted: true });
      }
    });

    return local.bulkDocs(updates);
  };

  const list = async seq => {
    const currentDiff = await diff(seq);
    if (isEmpty(diff)) return [];
    const { rows } = await local.allDocs({
      keys: Object.keys(currentDiff),
      include_docs: true,
    });
    return rows;
  };

  return Object.freeze({
    diff,
    revert,
    list,
  });
};
