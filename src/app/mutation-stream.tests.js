import { expect } from '@open-wc/testing';

import { createMutationStream } from './mutation-stream.js';

describe('mutation stream', async () => {
  it('should construct a working stream stream', async () => {
    const stream = createMutationStream(1);
    expect(stream.value()).to.equal(1);
  });

  it('should update the stream with a new value', async () => {
    const stream = createMutationStream(1);
    stream.set(2);
    expect(stream.value()).to.equal(2);
  });

  it('should only update with a mutated value', async () => {
    let i = 0;
    const stream = createMutationStream(1);
    stream.watch(() => {
      i += 1;
    });
    expect(i).to.equal(1);
    stream.set(2);
    expect(i).to.equal(2);
    stream.set(2);
    expect(i).to.equal(2);
  });
});
