import { expect } from '@open-wc/testing';
import { createChangesStream, onChangeRegistered } from './changes-stream.js';
import {
  ensureTestDbIsRunning,
  newRemoteDb,
  sleep,
} from '../../test/helpers/index.js';
import { fetchOrCreateDocument, compareRevs } from '../util/index.js';

describe('changes stream module', async () => {
  let db;

  before(async () => {
    await ensureTestDbIsRunning();
  });

  beforeEach(async () => {
    if (db) {
      await db.destroy();
    }
    db = newRemoteDb();
  });

  it('should be able to be created', async () => {
    const stream = createChangesStream(db);
    expect(stream).to.exist;
  });

  it('should register a change', async () => {
    const stream = createChangesStream(db);
    const doc = { _id: 'test_doc' };

    await db.put(doc);

    stream.watch(change => {
      expect(change).to.not.be.undefined;
      expect(change).to.satisfy(c => compareRevs(c, doc));
    });

    await sleep(50);
  });

  it('should not register a change after cancelled', async () => {
    const stream = createChangesStream(db);
    let i = 0;

    await db.put({ _id: 'test_doc' });

    stream.watch(change => {
      i += 1;
    });

    await sleep(50);

    stream.cancel();

    await db.put({ _id: 'test_doc2' });

    await sleep(50);

    expect(i).to.equal(1);
  });
});

describe('onChangeRegistered', async () => {
  const TEST_DOC = 'test_doc';
  let db;
  let changes;

  before(async () => {
    await ensureTestDbIsRunning();
  });

  beforeEach(async () => {
    if (db) {
      await db.destroy();
    }
    db = newRemoteDb();
    changes = createChangesStream(db);
  });

  it('should throw an error when missing id', async () => {
    let error;
    try {
      await onChangeRegistered({ name: 'bad' }, changes);
    } catch (err) {
      error = err;
    }
    expect(error).to.be.an('error');
  });

  it('should timeout when there is no change', async () => {
    let error;
    const initDoc = { _id: TEST_DOC, name: TEST_DOC };

    try {
      await onChangeRegistered(initDoc, changes, 50);
    } catch (err) {
      error = err;
    }

    expect(error).to.be.an('error');
  });

  it('should resolve when a new doc is created', async () => {
    const doc = { _id: TEST_DOC, name: TEST_DOC };
    const promise = onChangeRegistered(doc, changes);
    db.put(doc);

    promise.then(changed => {
      const equal = compareRevs(doc, changed);
      expect(equal).to.equal(true);
    });

    return promise;
  });

  it('should resolve when existing doc is updated', async () => {
    const doc = await fetchOrCreateDocument(db)({
      _id: TEST_DOC,
      name: TEST_DOC,
    });

    const newDoc = { ...doc, name: 'updated' };
    const promise = onChangeRegistered(newDoc, changes);
    db.put(newDoc);

    promise.then(changed => {
      const equal = compareRevs(newDoc, changed);
      expect(equal).to.equal(true);
    });

    return promise;
  });
});
