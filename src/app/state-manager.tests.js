import { expect } from '@open-wc/testing';
import { createStateManager } from './state-manager.js';

describe('app', () => {
  const initState = {
    name: 'initState',
    nested: {
      object: {
        name: 'nested',
      },
    },
  };

  it('should be able to be created', () => {
    const state = createStateManager(initState);
    expect(state).to.exist;
  });

  it('should be able to be watched', () => {
    const state = createStateManager(initState);
    let i = 0;
    state.watch(state => {
      i += 1;
      switch (i) {
        case 1:
          expect(state.name).to.equal('initState');
          break;
        case 2:
          expect(state.name).to.equal('updated');
          break;
      }
    });
    state.patch({ name: 'updated' });
    expect(i).to.equal(2);
  });

  it('should correctly apply patches', () => {
    const state = createStateManager(initState);
    expect(state.value().name).to.equal('initState');
    expect(state.value().nested.object.name).to.equal('nested');
    state.patch({ nested: { object: { name: 'updated' } } });
    expect(state.value().name).to.equal('initState');
    expect(state.value().nested.object.name).to.equal('updated');
  });
});
