import { isObject } from '../util/index.js';

export const createActions = (defaultActions, domainActions, store, patch) => {
  const actions = { ...defaultActions, ...domainActions };
  const boundActions = {};
  if (actions && isObject(actions)) {
    Object.keys(actions).forEach(k => {
      boundActions[k] = actions[k](store, patch);
    });
  }
  boundActions.store = store;
  return Object.freeze(boundActions);
};
