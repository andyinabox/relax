import { expect } from '@open-wc/testing';
import { createChangesStream } from './changes-stream.js';
import { createDocStream } from './doc-stream.js';
import { compareRevs } from '../util/index.js';
import {
  ensureTestDbIsRunning,
  newRemoteDb,
  sleep,
} from '../../test/helpers/index.js';

const TEST_DOC = 'test_doc';

describe('doc stream module', async () => {
  let db;
  let changes;

  before(async () => {
    await ensureTestDbIsRunning();
  });

  beforeEach(async () => {
    db = newRemoteDb();
    db.destroy();
    db = newRemoteDb();
    changes = createChangesStream(db);
  });

  it('should be able to be created', async () => {
    const stream = createDocStream(db, changes, { _id: TEST_DOC });
    expect(stream).to.exist;
  });

  it('should throw an error when missing initDoc id', async () => {
    let error;
    try {
      createDocStream(db, {});
    } catch (err) {
      error = err;
    }
    expect(error).to.be.an('error');
  });

  it('should create a missing doc', async () => {
    const initDoc = { _id: TEST_DOC, name: 'harry' };
    const stream = createDocStream(db, changes, initDoc);

    stream.watch(doc => {
      expect(doc._id).to.equal(TEST_DOC);
      expect(doc._rev).to.be.a('string');
      expect(doc).to.satisfy(d => compareRevs(d, initDoc));
      expect(doc).to.eql(stream.value());
    });
  });

  it('should fetch an existing doc', async () => {
    // create the doc
    let doc = { _id: TEST_DOC, name: TEST_DOC };
    await db.put(doc);
    doc = await db.get(doc._id);

    const stream = createDocStream(db, changes, { _id: TEST_DOC });

    stream.watch(newDoc => {
      expect(newDoc).to.eql(doc);
      expect(newDoc).to.eql(stream.value());
    });
  });

  it('should register changes via the api', async () => {
    const initDoc = { _id: TEST_DOC, name: TEST_DOC };
    const stream = createDocStream(db, changes, initDoc);

    let i = 0;

    stream.watch(change => {
      i += 1;
      switch (i) {
        case 1:
          expect(change).to.satisfy(d => compareRevs(d, initDoc));
          expect(change).to.eql(stream.value());
          stream.patch({ name: 'update' });
          break;
        case 2:
          expect(change.name).to.equal('update');
          break;
      }
    });

    return new Promise(r => window.setInterval(() => i === 2 && r(), 50));
  });

  it('should register changes via couchdb', async () => {
    const initDoc = { _id: TEST_DOC, name: TEST_DOC };
    const stream = createDocStream(db, changes, initDoc);

    let i = 0;

    stream.watch(change => {
      i += 1;
      switch (i) {
        case 1:
          expect(change).to.satisfy(d => compareRevs(d, initDoc));
          expect(change).to.eql(stream.value());
          db.put({ ...change, name: 'update' });
          break;
        case 2:
          expect(change.name).to.equal('update');
          break;
      }
    });

    return new Promise(r => window.setInterval(() => i === 2 && r(), 50));
  });
});
