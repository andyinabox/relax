import { createMutationStream } from './mutation-stream.js';

const defaults = {
  allowedRoles: ['editor'],
  allowedUsers: [],
};

export const isUserAuthorized = (
  userCtx,
  allowedRoles = [],
  allowedUsers = []
) => {
  // no user is authenticated
  if (!userCtx || !userCtx.name) return false;

  // user is authenticated and authorized
  if (allowedUsers.indexOf(userCtx.name) !== -1) {
    return true;
  }

  // i like to use a for loop here so i can return
  // immediately when a match is found
  for (let i = 0; i < userCtx.roles.length; i += 1) {
    if (allowedRoles.indexOf(userCtx.roles[i]) !== -1) {
      return true;
    }
  }

  return false;
};

export const createAuth = (db, options = {}) => {
  const { allowedRoles, allowedUsers } = { ...defaults, ...options };

  const authentication = createMutationStream(false);

  const login = async (username, password) => {
    await db.login(username, password);
    authentication.set(true);
  };

  const logout = async () => {
    await db.logout();
    authentication.set(false);
  };

  const check = async () => {
    const resp = await db.getSession();
    const auth = isUserAuthorized(resp.userCtx, allowedUsers, allowedRoles);
    authentication.set(auth);
  };

  const auth = Object.freeze({
    authenticated: authentication.value,
    watch: authentication.watch,
    login,
    logout,
    // "update" might be a better name
    check,
  });

  return auth;
};
