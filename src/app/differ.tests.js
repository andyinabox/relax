import { expect } from '@open-wc/testing';
import { createDiffer } from './differ.js';
import {
  ensureTestDbIsRunning,
  newLocalDB,
  newRemoteDb,
} from '../../test/helpers';

describe('differ', async () => {
  let local;
  let remote;

  before(async () => {
    await ensureTestDbIsRunning();
  });

  beforeEach(async () => {
    if (local) {
      await local.destroy();
    }
    local = newLocalDB();

    if (remote) {
      await remote.destroy();
    }
    remote = newRemoteDb();
  });

  it('should be able to be created', async () => {
    const differ = createDiffer(local, remote);
    expect(differ).to.exist;
  });
});
