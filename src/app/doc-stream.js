import flyd from 'flyd';
import merge from 'mergerino';
import { onChangeRegistered } from './changes-stream.js';
import {
  createStreamWatcher,
  createStreamFilter,
  compareRevs,
  fetchOrCreateDocument,
} from '../util/index.js';

export const createDocStream = (db, changesStream, initDoc) => {
  if (!initDoc._id) throw new Error('id must be set in initDoc');

  // setup the streams
  const docStream = flyd.stream();
  const errorsStream = flyd.stream();

  const isInitialized = new Promise((resolve, reject) => {
    flyd.on(change => {
      if (compareRevs(change, initDoc)) resolve(change);
    }, docStream);
    fetchOrCreateDocument(db)(initDoc).catch(reject);
  });

  // update docstream when change is recieved
  changesStream.filter(
    d => d._id === initDoc._id,
    d => docStream(d)
  );

  const patchDoc = async patch => {
    await isInitialized;
    const doc = merge(docStream(), patch);
    const docChanged = onChangeRegistered(doc, changesStream);

    try {
      const resp = await db.put(doc);
      if (!resp.ok)
        throw new Error(`error updating doc for docStream: ${resp.message}`);
    } catch (err) {
      errorsStream(err);
    }
    return docChanged;
  };

  const value = () => docStream();
  const patch = patchDoc;
  const watch = createStreamWatcher(docStream);
  const errors = createStreamWatcher(errorsStream);
  const filter = createStreamFilter(docStream);

  return Object.freeze({
    value,
    patch,
    watch,
    errors,
    filter,
  });
};
