import { expect } from '@open-wc/testing';
import { createActions } from './actions.js';
import { getStoreMock } from '../../test/helpers/index.js';

describe('createActions', () => {
  let store, patch, count;

  beforeEach(() => {
    count = 0;
    store = getStoreMock();
    patch = () => (count += 1);
  });

  const defaultActions = {
    defaultAction1: (s, p) => () => {
      expect(s).to.eql(store);
      expect(p).to.equal(patch);
      p();
    },
  };

  const domainActions = {
    domainAction1: (s, p) => () => {
      expect(s).to.eql(store);
      expect(p).to.eql(patch);
      p();
    },
  };

  it('binds the actions correctly', () => {
    const actions = createActions(defaultActions, domainActions, store, patch);
    expect(Object.keys(actions)).to.have.lengthOf(3);
    expect(actions).to.have.property('defaultAction1');
    expect(actions).to.have.property('domainAction1');
    expect(actions).to.have.property('store');
    expect(actions.store).to.eql(store);
    actions.defaultAction1();
    actions.domainAction1();
    // make sure the functions invoked correctly
    expect(count).to.equal(2);
  });
});
