import flyd from 'flyd';
import { createStreamWatcher } from '../util/index.js';

export const createReplicator = (origin, dest) => {
  const errorsStream = flyd.stream();

  let replicator = null;
  const start = () => {
    replicator = origin.replicate.to(dest, {
      live: true,
      retry: true,
    });

    replicator.catch(errorsStream);
  };
  const stop = () => {
    if (replicator) {
      replicator.cancel();
      replicator = null;
    }
  };

  return Object.freeze({
    start,
    stop,
    errors: createStreamWatcher(errorsStream),
  });
};
