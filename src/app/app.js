import { createBrowserHistory } from 'history';
import { createStateManager } from './state-manager.js';
import { createStore } from './store.js';
import { createActions } from './actions.js';
import defaultActions from '../actions/index.js';

const defaults = {
  initState: {},
  actions: {},
  remote: {
    protocol: 'http',
    host: 'localhost:8000',
  },
  dbName: 'proto_live_editor',
  publicUsername: 'relax',
  publicPassword: 'relax',
};

export const createApp = (PouchDB, options = {}) => {
  const opts = { ...defaults, ...options };

  const {
    remote: { protocol, host },
    dbName,
    publicUsername,
    publicPassword,
    initState,
  } = opts;

  const remoteUrl = `${protocol}://${host}/${dbName}`;

  // set up repo
  const store = createStore(PouchDB, dbName, remoteUrl, {
    publicUsername,
    publicPassword,
  });

  // set up state manager
  const stateManager = createStateManager(initState);
  const { patch } = stateManager;

  // create actions object
  const actions = createActions(defaultActions, opts.actions, store, patch);

  // track history in app state
  const history = createBrowserHistory();
  history.listen(({ location }) => patch({ location }));

  // track auth status in state
  store.auth.watch(authenticated => patch({ authenticated }));

  // set initial history sate
  history.push(window.location.pathname);

  const watchState = fn => {
    fn(stateManager.value(), actions, patch);
    stateManager.watch(state => fn(state, actions, patch));
  };

  const watchDocs = fn => {
    // fire once after setup
    store.setup().then(() => fn(null, actions, patch));
    // then again when files are changed
    store.changesStream.watch(doc => fn(doc, actions, patch));
  };

  return Object.freeze({
    watchState,
    watchDocs,
    store,
    history,
    actions,
  });
};
