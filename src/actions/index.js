export const fetchChangedDocs = (store, patch) => async () => {
  const changes = await store.listChanges();
  patch({ changes });
};

export const saveDoc = store => async doc => {
  return store().put(doc);
};

export const removeDoc = store => async doc => {
  return store().remove(doc);
};

export const revertDoc = () => async () => {
  // not sure how this will work yet
};

export const revertAllDocs = store => async () => {
  await store.revert();
};

export const publishChangedDocs = store => async () => {
  await store.publish();
};

export const login = store => async (username, password) => {
  await store.auth.login(username, password);
};

export const logout = store => async () => {
  await store.auth.logout();
};

export default {
  fetchChangedDocs,
  saveDoc,
  removeDoc,
  revertAllDocs,
  publishChangedDocs,
  login,
  logout,
};
