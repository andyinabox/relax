import flyd from 'flyd';
import filter from 'flyd/module/filter';

export const createStreamWatcher = stream => fn => flyd.on(fn, stream);

export const createStreamFilter = stream => (filterFn, watchFn) => {
  return flyd.on(watchFn, filter(filterFn, stream));
};
