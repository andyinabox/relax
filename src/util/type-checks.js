export { default as isObject } from 'underscore/modules/isObject.js';
export { default as isArray } from 'underscore/modules/isArray.js';
export { default as isNumber } from 'underscore/modules/isNumber.js';
export { default as isEmpty } from 'underscore/modules/isEmpty.js';
