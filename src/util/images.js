export const preloadImg = url =>
  new Promise(resolve => {
    const img = new Image();
    img.onload = resolve;
    img.src = url;
  });

export const tryPreloadImg = async url =>
  new Promise((resolve, reject) => {
    // eslint-disable-next-line consistent-return
    const handleResp = resp => {
      if (!resp.ok) return reject(new Error('failed to load image'));
      preloadImg(url).then(resolve);
    };
    fetch(url)
      .then(handleResp)
      .catch(e => reject(e));
  });
