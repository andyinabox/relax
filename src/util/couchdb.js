import omit from 'underscore/modules/omit.js';
import isEqual from 'underscore/modules/isEqual.js';

export const compareRevs = (doc1, doc2, omitted = ['_rev', 'updatedAt']) => {
  return isEqual(omit(doc1, omitted), omit(doc2, omitted));
};

export const fetchOrCreateDocument = db => async initDoc => {
  let doc;
  try {
    doc = await db.get(initDoc._id);
    return doc;
  } catch (e) {
    if (e.status !== 404) throw e;
  }

  const resp = await db.put(initDoc);
  if (!resp.ok) throw new Error(resp);
  doc = initDoc;
  doc._rev = resp.rev;

  return doc;
};
