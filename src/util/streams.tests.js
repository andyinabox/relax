import flyd from 'flyd';
import { expect } from '@open-wc/testing';
import { createStreamWatcher, createStreamFilter } from '../util/index.js';

describe('util -> createStreamWatcher', () => {
  it('should respond to stream updates', () => {
    const input = [1, 2, 3];
    const output = [];
    const stream = flyd.stream();

    const watcher = createStreamWatcher(stream)(v => output.push(v));
    input.forEach(v => stream(v));

    expect(output.length).to.equal(input.length);
    input.forEach((v, i) => expect(output[i]).to.equal(v));
  });
});

describe('util -> createStreamFilter', () => {
  it('should respond to stream updates', () => {
    const input = [1, 2, 3];
    const output = [];
    const stream = flyd.stream();

    const filter = createStreamFilter(stream)(
      () => true,
      v => output.push(v)
    );
    input.forEach(v => stream(v));

    expect(output.length).to.equal(input.length);
    input.forEach((v, i) => expect(output[i]).to.equal(v));
  });

  it('should filter stream upates', async () => {
    const input = [1, 2, 3, 4, 5, 6];
    const output = [];
    const stream = flyd.stream();

    const filter = createStreamFilter(stream)(
      v => v % 2 === 0,
      v => output.push(v)
    );
    input.forEach(v => stream(v));

    expect(output.length).to.equal(3);
    expect(output[0]).to.equal(2);
    expect(output[1]).to.equal(4);
    expect(output[2]).to.equal(6);
  });
});
