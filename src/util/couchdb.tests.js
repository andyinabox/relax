import { expect } from '@open-wc/testing';
import {
  ensureTestDbIsRunning,
  newRemoteDb,
} from '../../test/helpers/index.js';
import { compareRevs, fetchOrCreateDocument } from './couchdb.js';

describe('util.compareRevs', () => {
  const equal = [
    {
      desc: 'same id, different rev',
      docs: [
        {
          _rev: '1',
          _id: 'id',
        },
        {
          _rev: '2',
          _id: 'id',
        },
      ],
    },
    {
      desc: 'includes equal objects',
      docs: [
        {
          _rev: '1',
          _id: 'id',
          object: {
            equal: true,
          },
        },
        {
          _rev: '2',
          _id: 'id',
          object: {
            equal: true,
          },
        },
      ],
    },
  ];
  const notEqual = [
    {
      desc: 'different id, different rev',
      docs: [
        {
          _rev: '1',
          _id: 'id',
        },
        {
          _rev: '2',
          _id: 'id2',
        },
      ],
    },
    {
      desc: 'includes unequal objects',
      docs: [
        {
          _rev: '1',
          _id: 'id',
          object: {
            equal: true,
          },
        },
        {
          _rev: '2',
          _id: 'id',
          object: {
            equal: false,
          },
        },
      ],
    },
  ];

  equal.forEach(({ desc, docs: [d1, d2] }) => {
    it(`should be equal: ${desc}`, () => {
      const eq = compareRevs(d1, d2);
      expect(eq).to.equal(true);
    });
  });

  notEqual.forEach(({ desc, docs: [d1, d2] }) => {
    it(`should NOT be equal: ${desc}`, () => {
      const eq = compareRevs(d1, d2);
      expect(eq).to.equal(false);
    });
  });
});

describe('util.fetchOrCreateDocument', async () => {
  const TEST_DOC = 'test_doc';
  let db;

  before(async () => {
    await ensureTestDbIsRunning();
  });

  beforeEach(async () => {
    if (db) {
      await db.destroy();
    }
    db = newRemoteDb();
  });

  it('should create a missing document', async () => {
    const doc = await fetchOrCreateDocument(db)({ _id: TEST_DOC });
    expect(doc).to.exist;
    expect(doc._id).to.equal(TEST_DOC);
  });

  it('should fetch an existing document', async () => {
    let doc = { _id: TEST_DOC, name: TEST_DOC };
    const resp = await db.put(doc);
    doc = await fetchOrCreateDocument(db)({ _id: TEST_DOC });

    expect(doc).to.exist;
    expect(doc._id).to.equal(TEST_DOC);
    expect(doc.name).to.equal(TEST_DOC);
    expect(doc._rev).to.equal(resp.rev);
  });
});
